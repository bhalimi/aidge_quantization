/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include <string>

#include "aidge/QuantPTQ.hpp"
#include "aidge/hook/Hook.hpp"
#include "aidge/graph/GraphView.hpp"

namespace py = pybind11;

namespace Aidge {
void init_QuantPTQ(py::module &m) {

    m.def("check_architecture", &checkArchitecture, py::arg("network"),     
    R"mydelimiter(
    Determine whether an input GraphView can be quantized or not.
    :param network: The GraphView to be checked.
    :type network: :py:class:`aidge_core.GraphView`
    :return: True if the GraphView can be quantized, else false.
    :rtype: bool
    )mydelimiter");

    m.def("insert_scaling_nodes", &insertScalingNodes, py::arg("network"),     
    R"mydelimiter(
    Insert a scaling node after each affine node of the GraphView.
    :param network: The GraphView containing the affine nodes.
    :type network: :py:class:`aidge_core.GraphView`
    )mydelimiter");

    m.def("normalize_parameters", &normalizeParameters, py::arg("network"),    
    R"mydelimiter(
    Normalize the parameters of each parametrized node, so that they fit in the [-1:1] range.
    :param network: The GraphView containing the parametrized nodes.
    :type network: :py:class:`aidge_core.GraphView`
    )mydelimiter");

    m.def("compute_ranges", &computeRanges, py::arg("network"), py::arg("input_dataset"),
    R"mydelimiter(
    Compute the value ranges of every affine node output, given an input dataset.
    :param network: The GraphView containing the affine nodes, on which the inferences are performed.
    :type network: :py:class:`aidge_core.GraphView`
    :param input_dataset: inputDataSet The input dataset, consisting of a vector of input samples.
    :type input_dataset: A list of :py:class:`aidge_core.Tensor`
    :return: A map associating each affine node name to it's corresponding output range.
    :rtype: dict
    )mydelimiter");

    m.def("normalize_activations", &normalizeActivations, py::arg("network"), py::arg("value_ranges"),
    R"mydelimiter(      
    Normalize the activations of each affine node so that it become equal to one.
    This is done by reconfiguring the scaling nodes, as well as rescaling the weights and biases tensors.
    :param network: The GraphView containing the affine nodes.
    :type network: :py:class:`aidge_core.GraphView`
    :param value_ranges: The node output value ranges computed over the calibration dataset. 
    :type value_ranges: list of float.
    )mydelimiter");

    m.def("quantize_normalized_network", &quantizeNormalizedNetwork, py::arg("network"), py::arg("nb_bits"),
    R"mydelimiter( 
    Quantize an already normalized (in term of parameters and activations) network.
    :param network: The GraphView to be quantized.
    :type network: :py:class:`aidge_core.GraphView`
    :param nb_bits: The desired number of bits of the quantization.
    :type nb_bits: int
    )mydelimiter");

    m.def("quantize_network", &quantizeNetwork ,py::arg("network"), py::arg("nb_bits"), py::arg("input_dataset"), py::arg("optimize_cliping") = false,
    R"mydelimiter(   
    Main quantization routine. Performs every step of the quantization pipeline.
    :param network: The GraphView to be quantized.
    :type network: :py:class:`aidge_core.GraphView`
    :param nb_bits: The desired number of bits of the quantization.
    :type nb_bits: int
    :param input_dataset: The input dataset on which the value ranges are computed. 
    :type input_dataset: list of :py:class:`aidge_core.Tensor`
    :param optimize_cliping: Whether to optimize the cliping values or not.
    :type optimize_cliping: bool
    )mydelimiter");

    m.def("get_weight_ranges", &getWeightRanges, py::arg("network"),
    R"mydelimiter(  
    Compute the weight ranges of every affine node. Provided for debuging purposes.
    :param network: graphView The GraphView containing the affine nodes.
    :type network: :py:class:`aidge_core.GraphView`
    :return: A map associating each affine node name to it's corresponding weight range.
    :rtype: dict
    )mydelimiter");

    m.def("clear_biases", &clearBiases, py::arg("network"),   
    R"mydelimiter(
    Clear the affine nodes biases. Provided form debugging purposes.
    :param network: The GraphView containing the affine nodes.
    :type network: :py:class:`aidge_core.GraphView`
    )mydelimiter");

    m.def("compute_scaling_histograms", &computeScalingHistograms, py::arg("value_ranges"), py::arg("nb_bins"), py::arg("network"), py::arg("input_dataset"), "compute scaling histogram"); 
    m.def("compute_best_clipping", &computeBestClipping, py::arg("histogram"), py::arg("nb_bits"), "compute the best clipping for an histogram"); 

    m.def("dev_ptq", &devPTQ, py::arg("network"), "dev ptq"); 
}

PYBIND11_MODULE(aidge_quantization, m) {
    init_QuantPTQ(m);
}

} // namespace Aidge
