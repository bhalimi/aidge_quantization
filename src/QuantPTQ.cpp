/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/QuantPTQ.hpp"

#include <algorithm>  // std::find, std::reverse
#include <cmath>      // std::round
#include <cstddef>    // std::size_t
#include <cstdint>    // std::uint8_t
#include <map>
#include <memory>
#include <set>
#include <string>
#include <utility>    // std::make_pair
#include <vector>

#include "aidge/data/Tensor.hpp"
#include "aidge/graph/GraphView.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/scheduler/SequentialScheduler.hpp"
#include "aidge/utils/Log.hpp"

#include "aidge/operator/Add.hpp"
#include "aidge/operator/Concat.hpp"
#include "aidge/operator/Conv.hpp"
#include "aidge/operator/ConvDepthWise.hpp"
#include "aidge/operator/FC.hpp"
#include "aidge/operator/Mul.hpp"
#include "aidge/operator/ReLU.hpp"
#include "aidge/operator/Scaling.hpp"
#include "aidge/recipes/Recipes.hpp"
#include "aidge/operator/MetaOperator.hpp"

namespace Aidge{

static std::string makeUniqueName(std::string baseName, std::shared_ptr<GraphView> graphView)
{
    std::set<std::string> existingNames;
    for (std::shared_ptr<Node> node : graphView->getNodes())
        existingNames.insert(node->name());

    bool isInside = (existingNames.find(baseName) != existingNames.end());

    if (!isInside)
        return baseName;

    int index = 1;
    std::string newName = baseName;
    while (isInside)
    {
        newName = baseName + "_" + std::to_string(index);
        isInside = (existingNames.find(newName) != existingNames.end());
        index++;
    }

    return newName;
}

static bool isAffine(std::shared_ptr<Node> node)
{
    std::set<std::string> affineNodeTypes({"FC", "Conv", "ConvDepthWise", "PaddedConv", "PaddedConvDepthWise"});
    return (affineNodeTypes.find(node->type()) != affineNodeTypes.end());
}

static bool isSeamless(std::shared_ptr<Node> node)
{
    std::set<std::string> seamlessNodeTypes({"Pad", "MaxPooling", "AvgPooling", "PaddedMaxPooling", "PaddedAvgPooling", "GlobalAveragePooling"});
    return (seamlessNodeTypes.find(node->type()) != seamlessNodeTypes.end());
}

bool checkArchitecture(std::shared_ptr<GraphView> graphView)
{
    std::set<std::string> otherNodeTypes({"Flatten", "Add", "Concat", "Softmax", "ReLU", "Producer"});

    for (std::shared_ptr<Node> node : graphView->getNodes())
    {
        bool isOther = otherNodeTypes.find(node->type()) != otherNodeTypes.end();
        if (!isOther && !isAffine(node) && !isSeamless(node)) {
            Log::info(" GraphView can't be quantized : node type {} is not supported !", node->type());
            return false;
        }
    }

    return true;
}

static std::shared_ptr<Node> getFirstNode(std::shared_ptr<GraphView> graphView)
{
    std::shared_ptr<Node> currNode = graphView->rootNode();
    if (currNode->type() == "Producer")
        currNode = *(currNode->getChildren()).begin();

    std::shared_ptr<Node> parentNode = currNode->getParent(0);
    while (parentNode->type() != "Producer") {
        currNode = parentNode;
        parentNode = currNode->getParent(0);
    }

    return currNode;
}

static std::shared_ptr<Node> getLastNode(std::shared_ptr<GraphView> graphView)
{
    std::shared_ptr<Node> currNode = graphView->rootNode();
    while (currNode->getChildren().size() != 0)
        currNode = (*currNode->getChildren().begin());

    return currNode;
}

static void popSoftMax(std::shared_ptr<GraphView> graphView)
{
    std::shared_ptr<Node> lastNode = getLastNode(graphView);
    if (lastNode->type() == "Softmax") {
        graphView->replace({lastNode}, {}); // remove does not work !!!
    }
}

static void fillTensor(std::shared_ptr<Tensor> tensor, float value)
{
    // Get the tensor data pointer
    float * castedTensor = static_cast <float *> (tensor->getImpl()->rawPtr());

    // Fill the tensor
    for(std::size_t i = 0; i < tensor->size(); i++)
        castedTensor[i] = value;
}

static void rescaleTensor(std::shared_ptr<Tensor> tensor, float scaling)
{
    // Get the tensor data pointer
    float * castedTensor = static_cast <float *> (tensor->getImpl()->rawPtr());

    // Rescale the tensor
    for(std::size_t i = 0; i < tensor->size(); i++)
        castedTensor[i] *= scaling;
}

static void roundTensor(std::shared_ptr<Tensor> tensor)
{
    // Get the tensor data pointer
    float * castedTensor = static_cast <float *> (tensor->getImpl()->rawPtr());

    // Rescale the tensor
    for(std::size_t i = 0; i < tensor->size(); i++)
        castedTensor[i] = std::round(castedTensor[i]);
}

static float getTensorAbsoluteMax(std::shared_ptr <Tensor> tensor)
{
    // Get the tensor data pointer and edit it
    float * castedTensor = static_cast<float*>(tensor->getImpl()->rawPtr());

    // Get the tensor absolute max value
    float maxValue = 0.0f;
    for(std::size_t i = 0; i < tensor->size(); ++i) {
        if(std::fabs(castedTensor[i]) > maxValue) {
            maxValue = std::fabs(castedTensor[i]);
        }
    }
    return maxValue;
}

static void removeMatchingNodes(std::vector<std::shared_ptr<Node>>& nodeVector, std::string nodeType)
{
    std::vector<std::shared_ptr<Node>>::iterator iter = nodeVector.begin();
    while (iter != nodeVector.end())
    {
        if ((*iter)->type() == nodeType)
            iter = nodeVector.erase(iter);
        else
           ++iter;
    }
}

static void fixScheduling(std::vector<std::shared_ptr<Node>>& nodeVector) {

    std::vector<std::shared_ptr<Node>> correctedVector;

    for (int i = (nodeVector.size() - 1); i >= 0; --i)
    {
        std::shared_ptr<Node> node = nodeVector[i];
        bool isAlreadyInside = (std::find(correctedVector.begin(), correctedVector.end(), node) != correctedVector.end());
        if (!isAlreadyInside)
            correctedVector.push_back(node);
    }

    std::reverse(correctedVector.begin(), correctedVector.end());

    nodeVector = correctedVector;
}

static std::shared_ptr<Tensor> getWeightTensor(std::shared_ptr<Node> node)
{
    return std::static_pointer_cast<OperatorTensor>(node->getOperator())->getInput(1);
}

static std::shared_ptr<Tensor> getBiasTensor(std::shared_ptr<Node> node)
{
    return std::static_pointer_cast<OperatorTensor>(node->getOperator())->getInput(2);
}


void appendIdentity(std::shared_ptr<GraphView> graphView) {

    std::shared_ptr<Node> lastNode = getLastNode(graphView);

    int size = std::static_pointer_cast<OperatorTensor> (lastNode->getOperator())->getOutput(0)->size();

    std::shared_ptr<Node> identityNode = FC(size, size, true, makeUniqueName("Identity", graphView));
    identityNode->getOperator()->setDataType(DataType::Float32);
    identityNode->getOperator()->setBackend("cpu");

    std::shared_ptr<Tensor> weightTensor = std::static_pointer_cast<Tensor> (identityNode->getOperator()->getRawInput(1));
    fillTensor(weightTensor, 0);

    float * castedWeightTensor = static_cast<float *> (weightTensor->getImpl()->rawPtr());
    for (int n = 0; n < size; n++)
        castedWeightTensor[n + size * n] = 1.0;

    graphView->addChild(identityNode);
}


std::vector<std::shared_ptr<Node>> extractNodeVector(std::shared_ptr<GraphView> graphView, bool verbose)
{
    std::vector<std::shared_ptr<Node>> nodeVector;

    SequentialScheduler scheduler(graphView);
    scheduler.forward();
    nodeVector = scheduler.getStaticScheduling();

    //graphView->forwardDims();
    //scheduler.generateScheduling();
    //nodeVector = scheduler.getStaticScheduling();

    fixScheduling(nodeVector);

    removeMatchingNodes(nodeVector, "Producer");

    if (verbose)
    {
        Log::info("NB OF NODES = {}", nodeVector.size());
        for (std::shared_ptr<Node> node : nodeVector)
            Log::info("{} {}", node->type(), node->name());
    }

    //for (auto node : nodeVector)
    //    std::cout << node->type() << std::endl;

    return nodeVector;
}

void insertResidualNodes(std::shared_ptr<GraphView> graphView)
{
    std::vector<std::shared_ptr<Node>> nodeVector = extractNodeVector(graphView, false);

    for (std::shared_ptr<Node> node : nodeVector)
    {
        if (node->type() == "Add" || node->type() == "Concat")
        {
            int nbParents = node->getParents().size();
            for (int i = 0; i < nbParents; i++)
            {
                std::shared_ptr<Node> parentNode = node->getParent(i);
                bool parentIsForking = (parentNode->getChildren().size() > 1);
                if (parentIsForking)
                {
                    Log::info(" ### found residual branch at index {}", i);
                    Log::info(" ### inserting multiplicative node ...");

                    std::string residualNodeName = makeUniqueName(parentNode->name() + "_Res", graphView);
                    std::shared_ptr<Node> residualNode = Scaling(1.0, 0, false, residualNodeName);
                    residualNode->getOperator()->setDataType(DataType::Float32);
                    residualNode->getOperator()->setBackend("cpu");

                    graphView->insertParent(node, residualNode, i, 0, 0);
                }
            }
        }
    }

    graphView->forwardDims();
}


void insertScalingNodes(std::shared_ptr<GraphView> graphView)
{
    insertResidualNodes(graphView);

    std::set<std::shared_ptr<Node>> nodeSet = graphView->getNodes();

    for (std::shared_ptr<Node> node : nodeSet)
    {
        if (isAffine(node))
        {
            std::string scalingNodeName = makeUniqueName(node->name() + "_Scaling", graphView);
            std::shared_ptr<Node> scalingNode = Scaling(1.0, 0, false, scalingNodeName);
            scalingNode->getOperator()->setDataType(DataType::Float32);
            scalingNode->getOperator()->setBackend("cpu");

            if (node->getChildren().size() > 0)
            {
                std::shared_ptr<Node> nextNode = node->getChildren(0)[0];

                // XXX TODO : be extra careful about this ...
                int i = 0;
                while (nextNode->getParent(i) != node) i++;
                graphView->insertParent(nextNode, scalingNode, i, 0, 0);
            }
            else
            {
                // Log::info(" last node reached ! ");
                graphView->addChild(scalingNode);
            }
        }
    }

    graphView->forwardDims();

    // XXX Append identity if needed ...
    if (getLastNode(graphView)->type() == "Scaling")
        appendIdentity(graphView);

}


static std::shared_ptr<Node> getPreviousScalingNode(std::shared_ptr<Node> mergingNode)
{
    std::shared_ptr<Node> currNode = mergingNode;
    while(currNode->type() != "Scaling")
    {
        if (currNode->getParents().size() == 0)
        {
            Log::warn(" Warning : No previous Scaling node were found ! ");
            break;
        }
        currNode = currNode->getParents()[0];
    }
    return currNode;
}


// Be more careful about the '*' and '/' ...
void normalizeParameters(std::shared_ptr<GraphView> graphView)
{
    // CREATE THE ACCUMULATED RATIO MAP ///////////////////////////////////////

    std::vector<std::shared_ptr<Node>> nodeVector = extractNodeVector(graphView, false);

    std::map<std::string, float> accumulatedRatios;
    for (std::shared_ptr<Node> node : nodeVector)
        accumulatedRatios.insert(std::make_pair(node->name(), 1.0));

    // ITERATE OVER THE GRAPH /////////////////////////////////////////////////

    for (std::shared_ptr<Node> node : nodeVector)
    {
        // Scaling nodes still have a ratio of 1, so they are seamless ...
        if (node->type() == "ReLU" || node->type() == "Scaling" || isSeamless(node))
        {
            if (node != getFirstNode(graphView))
            {
                std::shared_ptr<Node> prevNode = node->getParent(0);
                accumulatedRatios[node->name()] = accumulatedRatios[prevNode->name()];
            }
        }

        // Residual nodes should enter in this category but their ratio is 1 ...
        if (isAffine(node))
        {
            // Rescale the weight tensor
            std::shared_ptr<Tensor> weightTensor = getWeightTensor(node);
            float scaling = getTensorAbsoluteMax(weightTensor);
            float ratio = 1.0 / scaling;
            rescaleTensor(weightTensor, ratio);

            // Accumulate the ratio
            if (node == getFirstNode(graphView))
                accumulatedRatios[node->name()] = ratio;
            else
            {
                std::shared_ptr<Node> prevNode = node->getParent(0);
                accumulatedRatios[node->name()] = accumulatedRatios[prevNode->name()] * ratio;
            }

            // Handle the bias ...
            bool nodeHasBias = (node->getParents().size() == 3);
            if (nodeHasBias)
            {
                std::shared_ptr<Tensor> biasTensor = getBiasTensor(node);
                // Check that a bias is present (as it is optional)
                if (biasTensor) {
                    rescaleTensor(biasTensor, accumulatedRatios[node->name()] );
                }
            }

        }

        if (node->type() == "Add" || node->type() == "Concat")
        {
            // We should assert if merging nodes are all scalings !
            std::vector<std::shared_ptr<Node>> mergingNodes = node->getParents();

            // Compute the max ratio ...
            float maxRatio = 0;
            for (std::shared_ptr<Node> mergingNode : mergingNodes)
            {
                float merginNodeRatio = accumulatedRatios[mergingNode->name()];
                if (merginNodeRatio > maxRatio)
                    maxRatio = merginNodeRatio;
            }

            accumulatedRatios[node->name()] = maxRatio;

            // Rescale the previous scaling Nodes
            for (std::shared_ptr<Node> mergingNode : mergingNodes)
            {
                float mergingNodeRatio = accumulatedRatios[mergingNode->name()];
                float rescaling = mergingNodeRatio / maxRatio;

                std::shared_ptr<Node> scalingNode = getPreviousScalingNode(mergingNode);

                std::shared_ptr<Scaling_Op> scalingOperator = std::static_pointer_cast<Scaling_Op> (scalingNode->getOperator());
                scalingOperator->scalingFactor() /= rescaling;
                accumulatedRatios[mergingNode->name()] /= rescaling; // optional ...
            }
        }
    }
}

std::map<std::string, float> computeRanges(std::shared_ptr<GraphView> graphView, std::shared_ptr<Tensor> inputTensor)
{
    std::map<std::string, float> valueRanges;

    SequentialScheduler scheduler(graphView);

    std::shared_ptr<Node> inputNode = getFirstNode(graphView);

    // Setup the input
    std::shared_ptr<Node> inputProducer = inputNode->getParent(0);
    inputProducer->getOperator()->setOutput(0, inputTensor);

    // Forward ...
    scheduler.forward();

    // Gather ranges ...

    std::set<std::shared_ptr<Node>> nodeSet = graphView->getNodes();
    for (std::shared_ptr<Node> node : nodeSet)
    {
        if (node->type() == "Scaling")  // XXX (node->type() != "Producer")
        {
            std::shared_ptr<Operator> nodeOperator = node->getOperator();
            std::shared_ptr<Tensor> valueTensor = std::static_pointer_cast<Tensor> (nodeOperator->getRawOutput(0));
            float range = getTensorAbsoluteMax(valueTensor);

            // Associate the value to the scaling node ...
            valueRanges.insert(std::make_pair(node->name(), range));
        }
    }

    return valueRanges;
}

std::map<std::string, float> computeRanges(std::shared_ptr<GraphView> graphView, std::vector<std::shared_ptr<Tensor>> inputDataSet)
{
    std::map<std::string, float> valueRanges;
    std::set<std::shared_ptr<Node>> nodeSet = graphView->getNodes();

    for (std::shared_ptr<Node> node : nodeSet)
        if (node->type() == "Scaling") // XXX (node->type() != "Producer")
            valueRanges.insert(std::make_pair(node->name(), 0));

    //int i = 0;
    for (std::shared_ptr<Tensor> sample : inputDataSet)
    {
        std::map<std::string, float> sampleRanges = computeRanges(graphView, sample);
        for (std::shared_ptr<Node> node : nodeSet)
        {
            if (node->type() == "Scaling")  // XXX (node->type() != "Producer")
            {
                std::string nodeName = node->name();
                if (sampleRanges[nodeName] > valueRanges[nodeName])
                    valueRanges[nodeName] = sampleRanges[nodeName];
            }
        }
    }

    return valueRanges;
}

void normalizeActivations(std::shared_ptr<GraphView> graphView, std::map<std::string, float> valueRanges)
{
    // CREATE THE SCALING FACTOR MAP //////////////////////////////////////////

    std::vector<std::shared_ptr<Node>> nodeVector = extractNodeVector(graphView, false);

    std::map<std::string, float> scalingFactors;

    for (std::shared_ptr<Node> node : nodeVector)
        scalingFactors.insert(std::make_pair(node->name(), 1.0));

    // ITERATE OVER THE GRAPH /////////////////////////////////////////////////

    for (std::shared_ptr<Node> node : nodeVector)
    {
        // Seamless scaling factor propagation ...
        if (isAffine(node) || isSeamless(node) || node->type() == "ReLU")
        {
            if (node == getFirstNode(graphView))
            {
                scalingFactors[node->name()] = 1.0;
            }
            else
            {
                std::shared_ptr<Node> prevNode = node->getParent(0);
                scalingFactors[node->name()] = scalingFactors[prevNode->name()];
            }
        }

        if (node->type() == "Scaling")
        {
            // Retreive the previous scaling factor ...
            std::shared_ptr<Node> prevNode = node->getParent(0);
            float prevScalingFactor = scalingFactors[prevNode->name()];

            // XXX HERE : valueRanges must contains all the scaling nodes !!!
            float scalingFactor = valueRanges[node->name()];

            std::shared_ptr<Scaling_Op> scalingOperator = std::static_pointer_cast<Scaling_Op> (node->getOperator());
            scalingOperator->scalingFactor() /= (scalingFactor / prevScalingFactor);

            scalingFactors[node->name()] = scalingFactor;

            // Fix the bias ...
            bool prevNodeHasBias = (prevNode->getParents().size() == 3);
            if (prevNodeHasBias)  {
                std::shared_ptr<Tensor> biasTensor = getBiasTensor(prevNode);
                rescaleTensor(biasTensor, 1.0 / prevScalingFactor);
            }
        }

       if (node->type() == "Concat" || node->type() == "Add")
        {
            // We should assert if merging nodes are all scalings !
            std::vector<std::shared_ptr<Node>> mergingNodes = node->getParents();

            // Compute the max scaling ...
            float maxScaling = 0;
            int maxNodeIndex = 0;
            for (std::size_t i = 0; i < mergingNodes.size(); i++)
            {
                float merginNodeScaling = scalingFactors[mergingNodes[i]->name()];
                if (merginNodeScaling > maxScaling) {
                    maxScaling = merginNodeScaling;
                    maxNodeIndex = i;
                }
            }

            // Ensure that the adding node does not overflow ...
            if (node->type() == "Add") {
                std::shared_ptr<Node> maxNode = mergingNodes[maxNodeIndex];
                maxScaling /= valueRanges[getPreviousScalingNode(maxNode)->name()];
                maxScaling *= valueRanges[getPreviousScalingNode(node)->name()];
            }

            scalingFactors[node->name()] = maxScaling;

            for (std::shared_ptr<Node> mergingNode : mergingNodes)
            {
                float mergingNodeScaling = scalingFactors[mergingNode->name()];
                float rescaling = mergingNodeScaling / maxScaling;

                std::shared_ptr<Node> scalingNode = getPreviousScalingNode(mergingNode);
                //Log::info(" SCALING NODE : {} {}", scalingNode->type(), scalingNode->name());

                std::shared_ptr<Scaling_Op> scalingOperator = std::static_pointer_cast<Scaling_Op> (scalingNode->getOperator());
                scalingOperator->scalingFactor() *= rescaling;
            }
        }
    }
}

void quantizeNormalizedNetwork(std::shared_ptr<GraphView> graphView, std::uint8_t nbBits)
{
    float signedMax = (1 << (nbBits - 1)) - 1;

    // ITERATE OVER THE GRAPH /////////////////////////////////////////////////

    std::vector<std::shared_ptr<Node>> nodeVector = extractNodeVector(graphView, false);

    for (std::shared_ptr<Node> node : nodeVector)
    {
        // XXX should be removed when the Scaling issue is fixed !!!
        bool isLastIdentity = (node->type() == "FC") && (node == getLastNode(graphView));

        if (isAffine(node) && !isLastIdentity)
        {
            // Rescale the weight tensor
            std::shared_ptr<Tensor> weightTensor = getWeightTensor(node);
            rescaleTensor(weightTensor, signedMax);
            roundTensor(weightTensor);

            //  Rescale the bias tensor
            bool nodeHasBias = (node->getParents().size() == 3);
            if (nodeHasBias)  {
                std::shared_ptr<Tensor> biasTensor = getBiasTensor(node);
                rescaleTensor(biasTensor, signedMax * signedMax);
                roundTensor(biasTensor);
            }

            std::shared_ptr<Node> scalingNode = *(node->getChildren().begin());
            std::shared_ptr<Scaling_Op> scalingOperator = std::static_pointer_cast<Scaling_Op> (scalingNode->getOperator());
            scalingOperator->scalingFactor() /= signedMax;
            scalingOperator->quantizedNbBits() = nbBits;
        }
    }

    // Ensure that residual scaling nodes are also quantized ...
    for (std::shared_ptr<Node> node : nodeVector)
    {
        if (node->type() == "Scaling")
        {
            std::shared_ptr<Scaling_Op> scalingOperator = std::static_pointer_cast<Scaling_Op> (node->getOperator());
            scalingOperator->quantizedNbBits() = nbBits;  // XXX HERE !!!
        }
    }
}

std::map<std::string, std::vector<int>> computeScalingHistograms(std::map<std::string, float> valueRanges, int nbBins, std::shared_ptr<GraphView> graphView, std::vector<std::shared_ptr<Tensor>> inputDataSet)
{
    //std::cout << " COMPUTING HISTOGRAMS ... " << std::endl;

    std::map<std::string, std::vector<int>> histograms;

    SequentialScheduler scheduler(graphView);

    std::shared_ptr<Node> inputNode = getFirstNode(graphView);

    // Setup the histograms ...

    for (std::shared_ptr<Node> node : graphView->getNodes())
    {
        if (node->type() == "Scaling")
        {
            std::vector<int> histogram;
            for (int i = 0; i < nbBins; i++)
                histogram.push_back(0);

            histograms.insert(std::make_pair(node->name(), histogram));
        }
    }

    // Fill the histograms ...

    for (std::shared_ptr<Tensor> inputTensor : inputDataSet)
    {
        // Setup the input
        std::shared_ptr<Node> inputProducer = inputNode->getParent(0);
        inputProducer->getOperator()->setOutput(0, inputTensor);

        // Forward ...
        scheduler.forward();

        // Gather values ...
        for (std::shared_ptr<Node> node : graphView->getNodes())
        {
            if (node->type() == "Scaling")
            {
                float valueRange = valueRanges[node->name()];

                std::shared_ptr<Operator> nodeOperator = node->getOperator();
                std::shared_ptr<Tensor> valueTensor = std::static_pointer_cast<Tensor> (nodeOperator->getRawOutput(0));

                float * castedTensor = static_cast<float *> (valueTensor->getImpl()->rawPtr());
                for(std::size_t i = 0; i < valueTensor->size(); i++)
                {
                    int bin = std::round(std::abs(castedTensor[i] / valueRange * nbBins));
                    histograms[node->name()][bin]++;
                }
            }
        }
    }

    return histograms;
}

float computeBestClipping(std::vector<int> histogram, std::uint8_t nbBits)
{
    //std::cout << " TEST " << std::endl;

    int nbBins = histogram.size();
    int nbIter = 100;
    int signedMax = (1 << (nbBits - 1)) - 1;

    // Compute the cumulative approximation error :
    // At each iteration we test a clipping candidate and loop over
    // the histogram to accumulate the squared error

    std::vector<float> clippingErrors;
    for (int it = 0; it < nbIter; it++)
    {
        // Compute the rounding cost of this particular clipping ...
        float acc = 0.0;
        float clipping = it / static_cast<float> (nbIter);
        for (int bin = 0; bin < nbBins; bin++)
        {
            float value = (bin + 0.5) / nbBins;
            float scaling = signedMax / clipping;
            float rounded = std::round(value * scaling) / scaling;
            float clipped = std::min(clipping, rounded);

            float approxError = (clipped - value);
            acc += (approxError * approxError) * histogram[bin];
        }
        clippingErrors.push_back(acc);
    }

    //for (int it = 0; it < nbIter; it++)
    //    std::cout << " it : "  << it  << " " << clippingErrors[it] << std::endl;

    float bestClipping = 0.0;
    float minClippingError = clippingErrors[0];
    for (int it = 0; it < nbIter; it++)
        if (clippingErrors[it] < minClippingError)
        {
            bestClipping = it / static_cast<float> (nbIter);
            minClippingError = clippingErrors[it];
        }

    return bestClipping;
}

void adjustRanges(std::map<std::string, float>& valueRanges, std::uint8_t nbBits, std::shared_ptr<GraphView> graphView, std::vector<std::shared_ptr<Tensor>> inputDataSet)
{
    //std::cout << " BEFORE CLIPING : " << std::endl;
    //std::map<std::string, float>::iterator it;
    //for (it = valueRanges.begin(); it != valueRanges.end(); it++)
    //    std::cout << it->first << " " << it->second << std::endl;

    int nbBins = (1 << (nbBits + 4)) ; // XXX Enhance this !!!

    std::map<std::string, std::vector<int>> histograms = computeScalingHistograms(valueRanges, nbBins, graphView, inputDataSet);

    for (std::shared_ptr<Node> node : graphView->getNodes())
        if (node->type() == "Scaling")
        {
            std::vector<int> histogram = histograms[node->name()];
            float cliping = computeBestClipping(histogram, nbBits);
            //std::cout << " cliping " << node->name() << " " << cliping << std::endl;
            valueRanges[node->name()] *= cliping;
        }

    //std::cout << " AFTER CLIPING : " << std::endl;
    //for (it = valueRanges.begin(); it != valueRanges.end(); it++)
    //    std::cout << it->first << " " << it->second << std::endl;
}

void quantizeNetwork(std::shared_ptr<GraphView> graphView, std::uint8_t nbBits, std::vector<std::shared_ptr<Tensor>> inputDataSet, bool OptimizeCliping)
{
    Log::info(" === QUANT PTQ 0.2.8 === ");

    if (!checkArchitecture(graphView))
        return;

    Log::info(" Removing the flatten nodes ... ");
    removeFlatten(graphView);

    Log::info(" Removing the Softmax node ... ");
    popSoftMax(graphView);

    Log::info(" Inserting the scaling nodes ...");
    insertScalingNodes(graphView);

    Log::info(" Normalizing the parameters ...");
    normalizeParameters(graphView);

    Log::info(" Computing the value ranges ...");
    std::map<std::string, float> valueRanges = computeRanges(graphView, inputDataSet);

    if (OptimizeCliping)
    {
        Log::info(" Optimizing the cliping values ...");
        adjustRanges(valueRanges, nbBits, graphView, inputDataSet);
    }

    Log::info(" Normalizing the activations ...");
    normalizeActivations(graphView, valueRanges);

    Log::info(" Quantizing the normalized network ...");
    quantizeNormalizedNetwork(graphView, nbBits);

    Log::info(" Network is quantized !");
}

std::map<std::string, float> getWeightRanges(std::shared_ptr<GraphView> graphView)
{
    std::map<std::string, float> weightRanges;

    for (std::shared_ptr<Node> node : graphView->getNodes())
    {
        if (isAffine(node))
        {
            std::shared_ptr<Tensor> weightTensor = getWeightTensor(node);
            float range = getTensorAbsoluteMax(weightTensor);
            weightRanges.insert(std::make_pair(node->name(), range));
        }
    }

    return weightRanges;
}

void clearBiases(std::shared_ptr<GraphView> graphView) {

    for (std::shared_ptr<Node> node : graphView->getNodes()) {
        if (node->type() == "FC" || node->type() == "Conv") {
            std::shared_ptr<Tensor> biasTensor = std::static_pointer_cast<OperatorTensor>(node->getOperator())->getInput(2);
            rescaleTensor(biasTensor, 0);
        }
    }
}

void devPTQ(std::shared_ptr<GraphView> graphView)
{
    for (std::shared_ptr<Node> node : graphView->getNodes())
        std::cout << " ### node : " <<  node->type() << std::endl;
}
}

