import gzip
import numpy as np
import matplotlib.pyplot as plt

import aidge_core
import aidge_backend_cpu
import aidge_onnx
import aidge_quantization

NB_SAMPLES  = 100 # max : 1000
NB_BITS     = 4

# --------------------------------------------------------------
# LOAD THE MODEL IN AIDGE
# --------------------------------------------------------------

aidge_model = aidge_onnx.load_onnx("assets/ConvNet.onnx", verbose=False)
aidge_core.remove_flatten(aidge_model)

# --------------------------------------------------------------
# LOAD THE SAMPLES / LABELS (NUMPY)
# --------------------------------------------------------------

samples = np.load(gzip.GzipFile('assets/mnist_samples.npy.gz', "r"))
labels  = np.load(gzip.GzipFile('assets/mnist_labels.npy.gz',  "r"))

# --------------------------------------------------------------
# SETUP THE AIDGE SCHEDULER
# --------------------------------------------------------------

# Create the Producer node
input_array  = np.zeros(784).astype('float32')
input_tensor = aidge_core.Tensor(input_array)
input_node = aidge_core.Producer(input_tensor, "X")

# Configuration for the inputs
input_node.get_operator().set_datatype(aidge_core.dtype.float32)
input_node.get_operator().set_backend("cpu")

# Link Producer to the Graph
input_node.add_child(aidge_model)

# Configuration for the model
aidge_model.set_datatype(aidge_core.dtype.float32)
aidge_model.set_backend("cpu")

# Create the Scheduler
scheduler = aidge_core.SequentialScheduler(aidge_model)

# --------------------------------------------------------------
# RUN SOME EXAMPLE INFERENCES WITH AIDGE
# --------------------------------------------------------------

def propagate(model, scheduler, sample):
    # Setup the input
    input_tensor = aidge_core.Tensor(sample)
    input_node.get_operator().set_output(0, input_tensor)
    # Run the inference
    scheduler.forward(verbose=False)
    # Gather the results
    output_node = model.get_output_nodes().pop()
    output_tensor = output_node.get_operator().get_output(0)
    return np.array(output_tensor)

def bake_sample(sample):
    sample = np.reshape(sample, (1, 1, 28, 28))
    return sample.astype('float32')

print('\n EXAMPLE INFERENCES :')
for i in range(10):
    input_array = bake_sample(samples[i])
    output_array = propagate(aidge_model, scheduler, input_array)
    print(labels[i] , ' -> ', np.round(output_array, 2))

# --------------------------------------------------------------
# COMPUTE THE MODEL ACCURACY
# --------------------------------------------------------------

def compute_accuracy(model, samples, labels):
    acc = 0
    for i, sample in enumerate(samples):
        x = bake_sample(sample)
        y = propagate(model, scheduler, x)
        if labels[i] == np.argmax(y):
            acc += 1
    return acc / len(samples)

accuracy = compute_accuracy(aidge_model, samples[0:NB_SAMPLES], labels)
print(f'\n MODEL ACCURACY : {accuracy * 100:.3f}%')

# --------------------------------------------------------------
# CREATE THE TENSOR SUBSET
# --------------------------------------------------------------

tensors = []
for sample in samples[0:NB_SAMPLES]:
    sample = bake_sample(sample)
    tensor = aidge_core.Tensor(sample)
    tensors.append(tensor)

# --------------------------------------------------------------
# APPLY THE PTQ TO THE MODEL
# --------------------------------------------------------------

aidge_quantization.quantize_network(aidge_model, NB_BITS, tensors)

# --------------------------------------------------------------
# UPDATE THE SCHEDULER
# --------------------------------------------------------------

scheduler = aidge_core.SequentialScheduler(aidge_model)

# --------------------------------------------------------------
# QUANTIZE THE INPUT TENSORS
# --------------------------------------------------------------

scaling = 2**(NB_BITS-1)-1
for i in range(NB_SAMPLES):
    samples[i] = np.round(samples[i]*scaling)

# --------------------------------------------------------------
# RUN SOME QUANTIZED INFERENCES WITH AIDGE
# --------------------------------------------------------------

print('\n EXAMPLE QUANTIZED INFERENCES :')
for i in range(10):
    input_array = bake_sample(samples[i])
    output_array = propagate(aidge_model, scheduler, input_array)
    print(labels[i] , ' -> ', np.round(output_array, 2))

# --------------------------------------------------------------
# COMPUTE THE MODEL ACCURACY
# --------------------------------------------------------------

accuracy = compute_accuracy(aidge_model, samples[0:NB_SAMPLES], labels)
print(f'\n QUANTIZED MODEL ACCURACY : {accuracy * 100:.3f}%')

# --------------------------------------------------------------
# WORK IS DONE !
# --------------------------------------------------------------

print('\n that\'s all folks !\n')
